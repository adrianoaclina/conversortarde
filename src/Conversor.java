import javax.swing.JOptionPane;

/**
 * 
 * Esta classe realiza a convers�o de temperaturas.
 * 
 * @author Adriano Aclina
 *
 */
public class Conversor {

	public static void main(String[] args) {
		
		// unidade de medida
		String unidade; 
		// temperatura em String
		String temp; 
		// temperatura em double
		double temperatura;
	
		// solicita ao usuario que digite uma
		// UNIDADE DE MEDIDA e armazena na variavel unidade
		unidade = JOptionPane.showInputDialog("Qual a unidade de medida ?");
		
		/* solicita ao usuario que digite uma
		 * temperatura
		 */
		temp = JOptionPane.showInputDialog("Qual a temperatura ?");
		
		// converte de String para double
		temperatura = Double.parseDouble(temp);
		
		// mensagem de convers�o realizada
		String msg;
		
		double celsius, fahrenheit, kelvin;
		
		switch (unidade) {
		case "C":
		case "c":
		case "Celsius":
		case "CELSIUS":	
		case "celsius":	
			// converte de �C para �F
			// F = (celsius * 1.8) + 32
			fahrenheit = (temperatura * 1.8) + 32;
			
			// converte de �C para �K
			// K = celsius + 273.15
			kelvin = temperatura + 273.15;
			
			// mensagem de sa�da
			msg = fahrenheit + "�F\n" + kelvin + " �K";
			// exibe o valor convertido em �C
			// para �F
			JOptionPane.showMessageDialog(null, msg);
			break;
		case "f":
		case "F":
		case "Fahrenheit":
		case "FAHRENHEIT":	
		case "fahrenheit":
			// CONVERTE �F para �C
			celsius = (temperatura - 32) / 1.8;
			// Converte �F para �K
	        fahrenheit = (1.8*(temperatura - 273))+ 32;
	        
	        kelvin = (temperatura + 459.67) * 0.55555556;
	        
	        msg = celsius + "�C\n" + kelvin + " �K";
	        JOptionPane.showMessageDialog(null, msg);
			break;
		case "k":
		case "K":
		case "Kelvin":
		case "KELVIN":
		case "kelvin":
			// CONVERTE �K para �F
			fahrenheit = temperatura * 1.8 - 459.67;
			// CONVERTE �K para �C
			celsius = temperatura - 273.15;
			
			msg = celsius + "�C\n" + fahrenheit + " �F";
	        JOptionPane.showMessageDialog(null, msg);
			break;
		default:
			System.out.println("Erro - Unidade Desconhecido");
			break;
		}
			
		}

}


